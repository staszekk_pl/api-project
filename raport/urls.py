from django.urls import include, path
from . import views

urlpatterns = [
    path('day/', views.generateRaportFromDay.as_view())
]
