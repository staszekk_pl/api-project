from rest_framework import generics
from project.models import Project, Task
from project.serializers import TaskSerializers
import datetime


class generateRaportFromDay(generics.ListCreateAPIView):
    now = datetime.datetime.now()
    sqlBuild = ('select * from project_task where status like "FINISH" and date = "'+ str(now.strftime("%Y-%m-%d")) +'"')
    queryset = Task.objects.raw(sqlBuild)
    serializer_class = TaskSerializers
