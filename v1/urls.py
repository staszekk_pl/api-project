from django.urls import include, path

urlpatterns = [
    path('users/', include('user.urls')),
    path('project/', include('project.urls')),
    path('raport/', include('raport.urls')),
    path('rest-auth/', include('rest_auth.urls')),
    path('rest-auth/registration/', include('rest_auth.registration.urls')),
]
