from rest_framework import serializers
from .models import Project, Task

class ProjectSerializers(serializers.ModelSerializer):
    class Meta:
        model = Project
        fields = ('name','author')

class TaskSerializers(serializers.ModelSerializer):
    class Meta:
        model = Task
        fields = ('name', 'descript', 'project', 'author', 'status', 'date')
