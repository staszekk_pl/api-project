from rest_framework import generics
from .models import Project, Task
from .serializers import ProjectSerializers,TaskSerializers

class ProjectDetailView(generics.ListCreateAPIView):
    queryset = Project.objects.all()
    serializer_class = ProjectSerializers

class TaskDetailView(generics.ListCreateAPIView):
    queryset = Task.objects.all()
    serializer_class = TaskSerializers

class TaskEditView(generics.UpdateAPIView):
    serializer_class = TaskSerializers
    def get_queryset(self):
      queryset = Task.objects.filter(pk=self.kwargs['pk'])
      return queryset

class TaskDeleteView(generics.DestroyAPIView):
    serializer_class = TaskSerializers
    def get_queryset(self):
      queryset = Task.objects.filter(pk=self.kwargs['pk'])
      return queryset
