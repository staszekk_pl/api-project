from django.urls import include, path
from . import views

urlpatterns = [
    path('projectList/', views.ProjectDetailView.as_view()),
    path('taskList/', views.TaskDetailView.as_view()),
    path('taskList/<int:pk>/edit/', views.TaskEditView.as_view(), name="pk"),
    path('taskList/<int:pk>/delete/', views.TaskDeleteView.as_view(), name="pk"),
]
