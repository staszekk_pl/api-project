# Generated by Django 2.1.7 on 2019-03-11 02:44

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('project', '0002_auto_20190311_0341'),
    ]

    operations = [
        migrations.DeleteModel(
            name='Status',
        ),
        migrations.AlterField(
            model_name='task',
            name='status',
            field=models.CharField(choices=[('NEW', 'Nowe'), ('PENDING', 'W trakcie'), ('FINISH', 'Wykonane'), ('ABANDEONED', 'odrzucone')], max_length=15),
        ),
    ]
