from django.conf import settings
from django.db import models
from django.utils.timezone import now
import datetime
# Create your models here.

class Project(models.Model):
    name = models.CharField(max_length=150)
    author = models.ForeignKey(settings.AUTH_USER_MODEL, on_delete=models.CASCADE)

    def __str__(self):
        return self.name

class Task(models.Model):
    name = models.CharField(max_length=150)
    descript = models.CharField(max_length=255)
    NEW = 'NEW'
    PENDING = "PENDING"
    FINISH = "FINISH"
    ABANDEONED = "ABANDEONED"
    statusChar = ((NEW,'Nowe'), (PENDING,'W trakcie'), (FINISH, 'Wykonane'), (ABANDEONED,'Odrzucone'))
    status = models.CharField(max_length=15 ,choices=statusChar)
    project = models.ForeignKey(Project, on_delete=models.CASCADE)
    author = models.ForeignKey(settings.AUTH_USER_MODEL, on_delete=models.CASCADE)
    date = models.DateField(("Date"), default=datetime.date.today)
    def __str__(self):
        return self.name
